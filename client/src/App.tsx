import SignInSide from './components/SignInSide';

function App() {
  return (
    <div>
      <SignInSide />
    </div>
  );
}

export default App;
